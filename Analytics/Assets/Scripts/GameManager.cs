﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics.Experimental;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

	private int numberOfMisses;
	private static int maxNumberOfTiles = 0;

	private float startingTime;

    private void Start()
    {
        LoadLevel( CurrentLevelNumber );
    }

    //This function is responsible for loading the level
	//It takes an integer levelNumber as parameter and reads an array of levels to get the correct one
	//Once that is done, it also gets the required symbols for that level by fetching through a list of Symbols
    private void LoadLevel( int levelNumber )
    {
		startingTime = Time.time;

        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );
		if (symbols.Count > maxNumberOfTiles)
		{
			maxNumberOfTiles = symbols.Count;
			var dict = new Dictionary<string, object>() { { "Number of Tiles", maxNumberOfTiles } };
			AnalyticsEvent.Custom("max-number-tiles-seen", dict);
		}
					
		//For each row and colum, sets the yPosition by checking the rowIndex multipliied by (1 + spacing between tiles)
		//Then applies the same idea for xPosition, and instantiates a tileprefab in those posisions. This will build the grid in the game
		//Then Randomizes a symbol, gets the renderer in each material and applies the texture of the randomized symbol
		//Once that is done, it initializes the tile passing the symbolIndex and removes the symbol index from the list so it keeps building the grid 
		//but without that symbol next time
		//Finally, Sets the camera to the correct position 
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    //Gets the required symbols to be placed in the tiles by passing a level and returns a List of Symbols to be applied in that level
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //Creates an array of all the symbols and stores them all in the array.
		//Checks the number of cards remaining based on cardTotal. If that total is not even, it throws an error
		//For each symbol in AllSymbols list, if that symbol is used, it adds the symbol to the local symbol list
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        // If that there are no symbols in the local symbols list, throw an error
		// If the count of the local list symbols is greater than the card total /2 , this means there are too many symbols for the number of cards
		// This is because there should be only total symbols = twice the number of cards at most
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        // It gets a repeat count that is equal to half the number of totalcards minus the number of symbols being used
		// IF this repeat count is greater than zero.
		// Creates a local list symbolsCopy and stores the local symbols list, and also an empty duplicateSymbols list
		// for each time we need to repeat a symbol, it randomizes one of those symbols from the local symbols List
		// once that is done, it adds to the duplicateSymbols list and removes from the symbolsCopy list at the same index.
		// If the count inf symbolsCopy is zero, it adds a new range symbols to the symbols copy for the next loop.
		// When the loop is dne, adds the duplicate symbols to the local symbols list as well
		// Finnally, returns the proper symbol list to be used in the tiles
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    //This calculates the offset to be used in the symbols.
	// Read the array of symbols, and for each symbol in the list it gets a value corresponded to the enum.
	// Once that is done, it returns a vector2 with the proper offset by doing math with the row/colums
	// If the symbol doesn't exist, returns zero.
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    //Sets the camera to have the proper ortographicSize and Position to match the grid and size of the puzzle.
	//This process takes spacing, number of rows and number of columns into account
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    //This function fires coroutines to handle the tiles and their flipping feedback.
	//It takes a tile, if there are no other tiles showing, it reveals that tile and sets it to be the first.
	//Otherwise, if there is no other secode tile showing, it taggs this tile as the second one, revals it,
	//and starts a coroutine passing if it's either a match or not as a parameter
    public void TileSelected( Tile tile )
    {
        if ( m_tileOne == null )
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    //Completes the Level. It adds one to the CurrentLevelNumber variable
	//If that number is greater than the number of levels available, game is over
	//Otherwise, loads the next level
    private void LevelComplete()
    {
        ++CurrentLevelNumber;

		AnalyticsEvent.LevelComplete(CurrentLevelNumber,"Level " + CurrentLevelNumber.ToString());
		var dict = new Dictionary<string, object>() { { "Level", CurrentLevelNumber } , { "Number of Misses", numberOfMisses} };
		AnalyticsEvent.Custom("number-of-misses", dict);

		int ellapsedTimeInLevel = (int)(Time.time - startingTime);
		var dict2 = new Dictionary<string, object>() { { "Level", CurrentLevelNumber } , { "Ellapsed Time", ellapsedTimeInLevel} };
		AnalyticsEvent.Custom("time-spent-in-level", dict2);

        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
		numberOfMisses = 0;
    }

    //Coroutine to Wait some time, check if it's a match and flip.
	//While the game is waiting, returns null. Otherwise, if it's a match, destroy both objects, and reduces te number of cards remaining
	//If it's not a match, hides the tiles, and set them to null.
	//Once there are no cards, completes the level
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
			++numberOfMisses;
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
            LevelComplete();
        }
    }

	private void Update()
	{
	}
	private void OnApplicationQuit()
	{
		var dict = new Dictionary<string, object>() { { "Player Quit Level On ", CurrentLevelNumber + 1 } };
		AnalyticsEvent.Custom("application-quit", dict);
	}
}
