﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //Start function. Gets the first child and stores in m_child, and ges the collider in this object and stores in m_collider.
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //Whenever the player clicks, it calls TileSelected passing this tile
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //On initialize, takes the game manager and a symbol to store in this tile.
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //When Reveal is called, all Coroutines are stopped, and it starts a new one to spin the tile, also disables the collider so it cant be clicked
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //Stops all coroutines, starts a new one to spins it back and this time enables the colider to be clicked.
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //Handles the spinning by taking a target and time for each spin.
	//Stores the initial rotation as well as its child eulerAngles
	//While the time is still there, it adds delta time.
	//It lerps the angle so it can create a nice spin between the starting rotation and the goal rotatoin.
	//Finnaly, it sets teh rotation to be exactly the target one to avoid decimals, and it sets the eulerAngles of the child as well.
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
